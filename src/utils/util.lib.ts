export class Util {
    public static safe(obj : any, key : string){
        return key.split('.').reduce((nestedObject, key) => {
            if(nestedObject && key in nestedObject) {
                return nestedObject[key];
            }
            return undefined;
        }, obj);
    }
}
