import {InitialState} from "../reducers/reducers";
import {WinnerService} from "./winner.service";
import {Grid} from "../models/grid.class";
import {GameActionTypes} from "../actions/actions.types";

export class ReducerService {
    public static actionSquareOnClick(state: InitialState, action: GameActionTypes): InitialState {

        state.historyService.resetHistoryTo(state.stepNumber);
        const current = state.historyService.getCurrent();
        const squares = current.squares.slice();

        if (!(WinnerService.calculateWinner(current) || squares[action.payload.index])) {
            squares[action.payload.index] = state.xIsNext ? 'X' : 'O';
            const newCurrent = new Grid(squares);
            state.historyService.addHistory(newCurrent);
            return Object.assign({}, state, {
                xIsNext: !state.xIsNext,
                stepNumber: state.stepNumber + 1
            });
        }

        return state;
    }

    public static actionGameOnJumpTo(state: InitialState, action: GameActionTypes) {

        state.historyService.resetHistoryTo(action.payload.step);
        return Object.assign({}, state, {
            stepNumber: action.payload.step,
            xIsNext: (action.payload.step % 2) === 0,
        });

    }
}



