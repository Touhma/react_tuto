import * as React from "react";

import {Grid} from "../models/grid.class";

export class HistoryService {

    private history = new Array<Grid>();

    constructor(){
        this.history.push(this.getNewGrid());
    }

    public getHistory(): Array<Grid> {
        return this.history;
    }

    public addHistory(grid: Grid) {
        this.history.push(grid);
    }

    public resetHistoryTo(step: number) {
        this.history.slice(0, step + 1);
    }

    public getCurrent(): Grid {
        let currentGrid = this.history[this.getHistory().length - 1];
        if (currentGrid) {
            return currentGrid;
        } else {
            return this.getNewGrid();
        }
    }

    public getStep(step: number): Grid {
        let stepGrid = this.history[step];
        if (stepGrid) {
            return stepGrid;
        } else {
            return this.getNewGrid();
        }
    }

    public getNewGrid(): Grid {
        return new Grid(Array(9).fill(''));
    }
}



