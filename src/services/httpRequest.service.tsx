import {Observable} from "rxjs";
import axios from 'axios';


export class HttpRequestService {
    public static get(api: string): Observable<any> {
        return new Observable((observer) => {
            axios.get(api)
                .then((response) => {
                    observer.next(response.data);
                    observer.complete();
                })
                .catch((error) => {
                    observer.error(error);
                });
        });
    }
}
