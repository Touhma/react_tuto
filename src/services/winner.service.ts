import {Grid} from "../models/grid.class";

export class WinnerService {

    public static calculateWinner(grid: Grid) {
        if(grid){
            const lines = [
                [0, 1, 2],
                [3, 4, 5],
                [6, 7, 8],
                [0, 3, 6],
                [1, 4, 7],
                [2, 5, 8],
                [0, 4, 8],
                [2, 4, 6],
            ];
            for (let i = 0; i < lines.length; i++) {
                const [a, b, c] = lines[i];
                if (grid.squares[a] && grid.squares[a] === grid.squares[b] && grid.squares[a] === grid.squares[c]) {
                    return grid.squares[a];
                }
            }
        }
        return null;
    }


}



