import {connect} from 'react-redux'
import {Game} from "../components/game";
import {InitialState} from "../reducers/reducers";

/*
const mapStateToProps = (state : any) => {{
 auth: state.auth
}};

export const AppContainer = connect(
    mapStateToProps
)(Game);

//*/

export const AppContainer = connect((state: InitialState) => ({
    historyService: state.historyService
}))(Game);
