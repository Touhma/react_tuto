import {ACTIONGAMEONJUMPTO, ACTIONSQUAREONCLICK, GameOnJumpToPayload, SquareOnClickPayload} from "./actions.types";

export function actionSquareOnClick(index: SquareOnClickPayload) {
    return {type: ACTIONSQUAREONCLICK, payload : index}
}

export function actionGameOnJumpTo(step: GameOnJumpToPayload) {
    return {type: ACTIONGAMEONJUMPTO, payload : step}
}

