export const ACTIONSQUAREONCLICK = 'ACTION_SQUARE_ONCLICK';
export const ACTIONGAMEONJUMPTO = 'ACTION_GAME_ONJUMPTO';

export interface SquareOnClickPayload {
    index: number
}

export interface GameOnJumpToPayload {
    step: number
}

interface ActionSquareOnClick {
    type: typeof ACTIONSQUAREONCLICK
    payload: SquareOnClickPayload
}

interface ActionGameOnJumpTo {
    type: typeof ACTIONGAMEONJUMPTO
    payload: GameOnJumpToPayload
}

export type GameActionTypes = ActionSquareOnClick & ActionGameOnJumpTo;
