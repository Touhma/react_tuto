import {GameActionTypes} from "../actions/actions.types";
import {HistoryService} from "../services/history.service";
import {ReducerService} from "../services/reducer.service";

export interface InitialState {
    xIsNext: boolean,
    stepNumber: number,
    historyService: HistoryService,
    status?: string
}

const _InitialState: InitialState = {
    xIsNext: true,
    stepNumber: 0,
    historyService: new HistoryService()
};

export function appReducer(
    state = _InitialState,
    action: GameActionTypes
): InitialState {
    switch (action.type) {
        case  'ACTION_SQUARE_ONCLICK':
            return ReducerService.actionSquareOnClick(state, action);
        case  'ACTION_GAME_ONJUMPTO':
            return ReducerService.actionGameOnJumpTo(state, action);
        default:
            return state;
    }
}
