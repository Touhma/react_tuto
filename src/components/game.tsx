import * as React from "react";

import {Board} from "./board";

import {WinnerService} from "../services/winner.service";

import {actionGameOnJumpTo, actionSquareOnClick} from "../actions/actions.creators";

import './game.scss';

export class Game extends React.Component<any> {

    render() {

        const {historyService, dispatch, stepNumber, xIsNext} = this.props;

        const history = historyService.getHistory();

        const moves = history.map((step: any, move: any) => {
            const desc = move ?
                'Go to move #' + move :
                'Go to game start';
            return (
                <li key={move}>
                    <button onClick={() => dispatch(actionGameOnJumpTo({step: move}))}>{desc}</button>
                </li>
            );
        });

        let status: string;
        const winner = WinnerService.calculateWinner(historyService.getStep(stepNumber));

        if (winner) {
            status = 'Winner: ' + winner;
        } else {
            status = 'Next player: ' + (xIsNext ? 'X' : 'O');
        }


        return (
            <div className="game">
                <div className="game-board">
                    <Board
                        grid={historyService.getStep(stepNumber)}
                        onClick={(i: number) => dispatch(actionSquareOnClick({index: i}))}/>
                </div>
                <div className="game-info">
                    <div className="status">{status}</div>
                    <ol>{moves}</ol>
                </div>
            </div>
        );
    }
}
