import * as React from "react";
import Square from "./square";
import './board.scss';
import {Grid} from "../models/grid.class";

import {QuotesService} from "../services/quotes.services";
import {Quote} from "../models/quote.class";

import {Util} from "../utils/util.lib";

interface IBoardProps {
    grid: Grid;
    onClick: Function;
}

interface IBoardState {
    quote?: Quote;
}

export class Board extends React.Component<IBoardProps, IBoardState> {

    private renderSquare(i: number) {
        return <Square value={this.props.grid.squares[i]}
                       onClick={() => this.props.onClick(i)}/>;
    }


    public componentDidMount() {
        QuotesService.getQuote().subscribe(quote => {
            console.log("done ! ");
            this.setState({
                quote: new Quote(quote.quote, quote.id)
            });
        });
    }

    public render() {

        return (
            <div>
                <div className="status">{Util.safe(this.state, 'quote.quote')}</div>

                <div className="board-row">
                    {this.renderSquare(0)}
                    {this.renderSquare(1)}
                    {this.renderSquare(2)}
                </div>
                <div className="board-row">
                    {this.renderSquare(3)}
                    {this.renderSquare(4)}
                    {this.renderSquare(5)}
                </div>
                <div className="board-row">
                    {this.renderSquare(6)}
                    {this.renderSquare(7)}
                    {this.renderSquare(8)}
                </div>
            </div>
        );
    }
}
