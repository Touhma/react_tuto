import * as React from "react";

import './square.scss';

interface ISquareProps {
    value: string;
    onClick(): any;
}

const Square = (props: ISquareProps) => {
    const {onClick : handleClick, value} = props;
    /*
    const x = {
        a: {
            b: {
                c: 'd'
            }
        },
        x: 25,
        y: 5
    };
    const { a, ...rest } = x;
    rest = { x: 25, y: 5 };
    const { ...rest, a } = y;
    y = {
        x: 25, y: 5,
        a
    }
    */
    return (
        <button className="square"
                onClick={handleClick}>
            {value}
        </button>
    )
};

export default Square;
