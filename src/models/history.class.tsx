import {Grid} from "./grid.class";

export class History {
    history : Array<Grid>;

    constructor() {
        this.history = new Array<Grid>();
    }
}
