export class Grid {
    public squares: Array<string>;

    constructor(squares: Array<string>) {
        this.squares = squares;
    }
}
