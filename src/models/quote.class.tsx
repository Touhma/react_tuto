export class Quote {
    quote : string;
    id : string;

    constructor(quote: string, id : string) {
        this.id = id;
        this.quote = quote;
    }
}
