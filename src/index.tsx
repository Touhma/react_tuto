import * as React from 'react';
import * as ReactDOM from 'react-dom';

import registerServiceWorker from './registerServiceWorker';
import {appReducer} from "./reducers/reducers";
import {createStore} from "redux";
import {Provider} from "react-redux";
import {AppContainer} from "./containers/app.container";

import './index.scss';

export const store = createStore(appReducer);

const App = () => (
    <Provider store={store}>
        <AppContainer />
    </Provider>
);

ReactDOM.render(<App />, document.getElementById('root'));

registerServiceWorker();
